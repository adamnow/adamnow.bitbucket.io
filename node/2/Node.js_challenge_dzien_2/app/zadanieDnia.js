const fs = require('fs');

let file = process.argv[2];

fs.readFile(file, 'utf8', (error, data) => {
   if(error) {
      console.log(error);
      return false;
   }

   data = data.split("");
   let newText = "";
   data.forEach((el,ind) => {
      if( ind % 2 == 0 ) {
         newText += el.toUpperCase();
      } else {
         newText += el.toLowerCase();
      }
   });

   fs.writeFile(file, newText, (error) => {
      if( error ) {
         console.log(error);
         return;
      }

      console.log("Zapisano nowy tekst");
   });

})