//Twój kod
const fs = require('fs');

fs.readFile('./data/zadanie01/input.json', 'utf8', (error, data) => {
   if( !error ) {
      const result = JSON.parse(data);
      let summary = 0;
      result.forEach((el, ind) => {
         summary += Number(el);
      })
      
      fs.writeFile('./data/zadanie01/sum.txt', summary, (error) => {
         if( error ) {
            console.log(error);
         } else {
            console.log("Zapisano porawnie");
         }
      })

   } else {
      console.log(error);
   }
});