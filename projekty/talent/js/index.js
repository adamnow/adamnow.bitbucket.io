let tInterval = .3;
const wow = () => {
    windowHeight = window.innerHeight;
    downPosition = windowHeight + window.scrollY;
    
    [...animElem].forEach((el,ind)=>{
        let pos = el.getBoundingClientRect();
        if( pos.top < ( windowHeight - ( windowHeight/3 ) ) ) {
            tInterval += .2;
                el.classList.add('wow-end');
                // el.style.transitionDelay = tInterval + 's';
            if( tInterval > .6 )
                tInterval = .3;
        }
    });
}
const langBtn = document.getElementById('show-lang-nav');
const body = document.body;
const animElem = document.querySelectorAll('.wow');
const tabs = document.querySelectorAll('.tab__nav');
const tabContent = document.querySelectorAll('.tab__content');
const mobileBtn = document.getElementById('mnav');
const arrow = document.querySelectorAll('.arrow');
const faq = document.querySelectorAll('.faq-q');
var windowHeight = window.innerHeight,
    downPosition = windowHeight;

if( faq ) {
    [...faq].forEach((elem)=>{
        elem.addEventListener('click',()=>{
            elem.classList.toggle('click-faq');
        })
    })
}

if( mobileBtn ) {
    mobileBtn.addEventListener('click',()=>{
        body.classList.toggle('mobile-nav-open');
    })
}

if( langBtn ) {
    langBtn.addEventListener('click', ()=>{
        body.classList.toggle('lang-open');
    });
}

wow();
if( animElem ) 
    window.addEventListener('scroll',wow )

// if( arrow ) {
//     let arrowArray = [];
//     let direction = 0;
//     document.getElementsByClassName('join')[0].addEventListener('mousemove',(e)=>{
//         let pos = e.layerY;
//         let dir = '+';
//         if( pos > direction ) {
//             dir = '+';
//         } else {
//             dir = '-';
//         }

//         [...arrow].forEach((el, ind)=>{
//             if( direction == '+' )
//                 var newValue = ( arrowArray[ind] || 0 ) + parseFloat(el.getAttribute('data-t'));
//             else
//                 var newValue = ( arrowArray[ind] || 0 ) - parseFloat(el.getAttribute('data-t'));
//             arrowArray[ind] = newValue;
//             console.log(arrowArray);
//         });
//         direction = pos;
//     })

// }



if( tabs ) {
    let id;
    [...tabs].forEach((el)=>{
        el.addEventListener('click', (event)=>{
            event.preventDefault();
            [...tabs].forEach((elem)=>{
                elem.classList.remove('active');
            });
            el.classList.add('active');
            id = el.getAttribute('data-tab');
            [...tabContent].forEach((tab)=>{
                tab.classList.remove('active');
            });

            [...document.querySelectorAll('[data-tab-image]')].forEach((iel)=>{
                iel.style.display = 'none';
            });
            
            document.querySelector('[data-target="'+id+'"]').classList.add('active');
            document.querySelector('[data-tab-image="'+id+'"]').style.display = 'block';

        });
    })
}

// console.log(document.getElementsByClassName('register')[0].getBoundingClientRect());

// magnific popup

// marqee
    $("[data-hide]").removeAttr('data-hide');
    $('.marquee').marquee({
        duration: 13000,
        //gap in pixels between the tickers
        gap: 50,
        //time in milliseconds before the marquee will start animating
        delayBeforeStart: 0,
        //'left' or 'right'
        direction: 'left',
        //true or false - should the marquee be duplicated to show an effect of continues flow
        duplicated: true
    });

    let video = `<video class='full-section__bg' id='v' poster="images/poster.jpg" src="video/movie.mp4" loop autoplay><source src="video/movie.mp4" type="video/mp4"></video>`;
    let poster = `<img src="images/poster.jpg" alt="" class='full-section__bg'>`;

    function setMedia() {
        let elem;
        if( window.innerWidth >= 1024 ) {
            elem = video;
        } else {
            elem = poster
        }
        if( document.getElementById('video-ctn') ) {
            document.getElementById('video-ctn').innerHTML = elem;
        }
    }

    setMedia();
    if( document.getElementById('v') ) {
        document.getElementById('v').play().then(()=>{

        });
    }

    // cookie
    window.addEventListener('load', function () {
        var cookieCtn = document.getElementById('cookie-p'),
            cookieClose = document.querySelector('.cookie-p__x'),
            cookieVal = localStorage.getItem('cookie_val_us') || null;
    
        if (!cookieVal) {
            cookieCtn.classList.add('show_cookie');
        }
    
        cookieClose.addEventListener('click', () => {
            localStorage.setItem('cookie_val_us', '1');
            cookieCtn.classList.remove('show_cookie');
        });
    
    });

    if( $('[video-url]').length )
        $('[video-url]').videoPopup({autoplay: 1,});
    